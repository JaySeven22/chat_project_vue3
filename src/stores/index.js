import { defineStore } from "pinia";

export const Store = defineStore('messageStore', {
    state: () => ({
        chat_messages: null,
        selectContact: null,
        messages_1: [
            { id: 1, text: 'Чат был обновлён', time: '19:48'},
        ],
        messages_2: [
            { id: 1, text: 'Ок, увидимся позже', time: '18:30'},
        ],
        messages_3: [
            { id: 2, text: 'Напомни, че там сделать надо', time: '18:16'},
        ],
        messages_4: [
            { id: 1, text: 'Погнали шары гонять', time: '18:02'},
        ],
        messages_5: [
            { id: 1, text: 'Завтра напомни мне по поводу динамиков', time: '17:42'},
        ],
        messages_6: [
            { id: 2, text: 'Все готово', time: '17:08'},
        ],
        messages_7: [
            { id: 1, text: 'Канал создан', time: '16:15'},
        ],
        messages_8: [
            { id: 1, text: 'Передай маме, что я буду к 8', time: 'Wed'},
        ],
        messages_9: [
            { id: 1, text: 'Поехали отдыхать🏝', time: 'Tue'},
        ],
        contacts: [
            { id: 1, name: 'Чат 1', image: '@/Avatar11.png', last_message: 'Чат был обновлён', time: '19:48', new_messages: 1, last_connection: 'в сети 5 минут назад', verifired: true},
            { id: 2, name: 'Чат 2', image: '@/Avatar22.png', last_message: 'Ок, увидимся позже', time: '18:30', new_messages: 2, last_connection: 'в сети 5 минут назад', verifired: false},
            { id: 3, name: 'Чат 3', image: '@/Avatar44.png', last_message: 'Вы: Напомни, че там сделать надо', time: '18:16', new_messages: 0, last_connection: 'в сети 5 минут назад', verifired: false},
            { id: 4, name: 'Чат 4', image: '@/Avatar33.png', last_message: 'Погнали шары гонять', time: '18:02', new_messages: 0, last_connection: 'в сети 5 минут назад', verifired: false},
            { id: 5, name: 'Чат 5', image: '@/Avatar4.svg', last_message: 'Завтра напомни мне по поводу ди...', time: '17:42', last_connection: 'в сети 5 минут назад', new_messages: 0, verifired: false},
            { id: 6, name: 'Чат 6', image: '@/Avatar5.svg', last_message: 'Вы: Все готово ', time: '17:08', new_messages: 0, last_connection: 'в сети 5 минут назад', verifired: false},
            { id: 7, name: 'Чат 7', image: '@/Avatar6.svg', last_message: 'Канал создан', time: '16:15', new_messages: 0, last_connection: 'в сети 5 минут назад', verifired: false},
            { id: 8, name: 'Чат 8', image: '@/Avatar7.svg', last_message: 'Передай маме, что я буду к 8', time: 'Wed', new_messages: 0, last_connection: 'в сети 5 минут назад', verifired: false},
            { id: 9, name: 'Чат 9', image: '@/Avatar8.svg', last_message: 'Поехали отдыхать🏝', time: 'Tue', new_messages: 0, last_connection: 'в сети 5 минут назад', verifired: false},
        ]
    }),
    actions: {

        async enterNewMessage(message) {
            let time = new Date()
            this.chat_messages.push({
                id: this.chat_messages.length + 1,
                text: message,
                time: `${time.getHours()}:${(time.getMinutes() < 10 ? '0' : '') + time.getMinutes()}`
            })
            this.chat_messages.id % 2 ? this.selectContact.last_message = message : this.selectContact.last_message = `Вы: ${message}`
        },

        async selectedContact(id) {
            this.selectContact = null
            this.selectContact = this.contacts.find(item => item.id == id)
            this.selectContact.new_messages = 0
        },

        async loadSelectMessages(id) {
            switch(id) {
                case 1:
                    return this.chat_messages = this.messages_1
                case 2:
                    return this.chat_messages = this.messages_2
                case 3:
                    return this.chat_messages = this.messages_3
                case 4:
                    return this.chat_messages = this.messages_4
                case 5:
                    return this.chat_messages = this.messages_5
                case 6:
                    return this.chat_messages = this.messages_6
                case 7:
                    return this.chat_messages = this.messages_7
                case 8:
                    return this.chat_messages = this.messages_8
                case 9:
                    return this.chat_messages = this.messages_9
            }
        },

    },
    getters: {
        
        getMessages: (state) => {
            return state.chat_messages
        },

        getContacts: (state) => {
            return state.contacts
        },

        getSelectContact: (state) => {
            return state.selectContact
        }
    }
})