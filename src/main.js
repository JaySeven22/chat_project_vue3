import { createApp } from 'vue'
import { createPinia } from 'pinia'
import './style.css'

import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

import App from './App.vue'

const pinia = createPinia()

const vuetify = createVuetify({
    iconfont: 'mdi',
    components,
    directives,
})
  
createApp(App).use(pinia).use(vuetify).mount('#app')
